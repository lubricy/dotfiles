#!/bin/sh

list="vim python lua zsh git tmux ln"
Check (){
    missing=""
    for i in $list
    do
        if type $i 2>/dev/null; then
            echo
        else
           missing="${missing} ${i}" 
        fi
    done
    if [[ -z $missing ]]; then
        return 0
    else
        return -1
    fi
}

if Check; then
    echo "All requirments satisfied."
else
    echo "Missing following command: ${missing}"
    echo "Please provide your package manager:"
    echo "Examples:"
    echo "\t$ brew install"
    echo "\t$ sudo pacman -S"
    read pkgmgr
    for i in $missing
    do
        $pkgmgr $i
    done
    sleep 2s
    if Check; then
        echo "Install packages finished."
    else
        echo "Still missing following command: ${missing}"
        echo "install pacakges failed."
        exit -1
    fi
fi

#echo "Copying dot files ..."

#rsync -abviuzP ./ ~/ --exclude-from "./exclude-files"
echo "linking dot files ..."
ln -sf `pwd`/.vim/ ~/
ln -sf `pwd`/.zshrc ~/
ln -sf `pwd`/.tmux.conf ~/
ln -sf `pwd`/.vimrc ~/
DIRECTORY=`pwd`/.vim/bundle/Vundle.vim
if [ ! -d "$DIRECTORY" ]; then
    echo "Installing Vundle.Vim ..."
    git clone https://github.com/VundleVim/Vundle.vim.git $DIRECTORY
else
    echo "Updating Vundle.Vim ..."
    git -C $DIRECTORY pull
fi

echo "Installing Vim Plugins ..."
vim +PluginInstall +qall
