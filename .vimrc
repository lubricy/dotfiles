source ~/.vim/plugins.vim

" NERDTree settings
" Open NERDTree automatically
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
"Key Mapping 
nmap <C-p> :NERDTreeToggle<CR>

"Gundo settings
nmap U :GundoToggle<CR>

"synastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_signs = 0
let g:syntastic_lua_checkers = ["luac", "luacheck"]
let g:syntastic_lua_luacheck_args = "--no-unused-args" 

"Neocomplete settings
source ~/.vim/neocomplete.vim

"xuhdev/vim-latex-live-preview settings
let g:livepreview_previewer = 'mupdf'

"Custom settings
syntax on
set number ruler
set tabstop=8 expandtab shiftwidth=4 softtabstop=4
set hlsearch incsearch
set smarttab
set smartindent
set showcmd
set pastetoggle=<F10>
nmap <Space> <C-d>
nmap <Up> gk
nmap <Down> gj
nmap <silent> <Up> :wincmd k<CR>
nmap <silent> <Down> :wincmd j<CR>
nmap <silent> <Left> :wincmd h<CR>
nmap <silent> <Right> :wincmd l<CR>
"The sudo tewak
command W :execute ':silent w !sudo tee % > /dev/null' | :edit!
"cmap W w !sudo tee % >/dev/null<CR>

" Jump to the next or previous line that has the same level or a lower
" level of indentation than the current line.
"
" exclusive (bool): true: Motion is exclusive
" false: Motion is inclusive
" fwd (bool): true: Go to next line
" false: Go to previous line
" lowerlevel (bool): true: Go to line with lower indentation level
" false: Go to line with the same indentation level
" skipblanks (bool): true: Skip blank lines
" false: Don't skip blank lines
function! NextIndent(exclusive, fwd, lowerlevel, skipblanks)
  let line = line('.')
  let column = col('.')
  let lastline = line('$')
  let indent = indent(line)
  let stepvalue = a:fwd ? 1 : -1
  while (line > 0 && line <= lastline)
    let line = line + stepvalue
    if ( ! a:lowerlevel && indent(line) == indent ||
          \ a:lowerlevel && indent(line) < indent)
      if (! a:skipblanks || strlen(getline(line)) > 0)
        if (a:exclusive)
          let line = line - stepvalue
        endif
        exe line
        exe "normal " column . "|"
        return
      endif
    endif
  endwhile
endfunction

function! FindIndent(fwd,strict)
    let line = line('.')
    let column = col('.')
    let lastline = line('$')
    let indent = indent(line)
    let stepvalue = a:fwd ? 1 : -1
    while (line > 0 && line <= lastline)
        let line = line + stepvalue
        if((!a:strict && indent(line) < indent) || (a:strict && indent(line) != indent))
            if(strlen(getline(line) > 0))
                break 
            endif
        endif
    endwhile
    let line = line - stepvalue
    exe line
    exe "normal " column . "|"
endfunction


function! FindSameIndent(fwd)
    let ml = line('.')
    let bl = ml
    let column = col('.')
    let lastline = line('$')
    let indent = indent(ml)
    let stepvalue = a:fwd ? 1 : -1
    while (ml > 0 && ml <= lastline)
        let ml = ml + stepvalue 
        if ((indent(ml) != indent) && (strlen(getline(ml))>0) )
            break
        endif
    endwhile
    let ml = ml - stepvalue

    if(bl == ml)
        while (ml > 0 && ml <= lastline)
            let ml = ml + stepvalue
            if(indent(ml) <= indent)
                if(strlen(getline(ml))>0)
                    break

                endif
            endif
        endwhile
    endif
    exe ml
    exe "normal " column . "|"
endfunction

function! FindOutIndent(fwd)
    let ml = line('.')
    let column = col('.')
    let lastline = line('$')
    let indent = indent(ml)
    let stepvalue = a:fwd ? 1 : -1
    while (ml > 0 && ml <= lastline)
        let ml = ml + stepvalue
        if(indent(ml) < indent)
            if(strlen(getline(ml))>0)
                break
            endif
        endif
    endwhile
    let ml = ml - stepvalue
    exe ml
    exe "normal " column . "|"
endfunction


function! IndentInnerMotion(count)
    let i = 0
    while (i < a:count)
        let i = i + 1
        call FindSameIndent(0)
    endwhile
    normal! V
    while (i > 0)
        let i= i - 1
        call FindSameIndent(1)
    endwhile
endfunction

function! IndentOuterMotion(count)
    let i = 0
    while (i < a:count)
        let i = i + 1
        call FindOutIndent(0)
    endwhile
    normal! V
    while (i > 0)
        let i= i - 1
        call FindOutIndent(1)
    endwhile
endfunction

" Moving back and forth between lines of same or lower indentation.

nnoremap <silent> ]l :call FindSameIndent(1)<CR>
nnoremap <silent> [l :call FindSameIndent(0)<CR>
nnoremap <silent> ]L :call FindOutIndent(1)<CR>
nnoremap <silent> [L :call FindOutIndent(0)<CR>
vnoremap <silent> ]l <Esc>:call FindSameIndent(1)<CR>m'gv''
vnoremap <silent> [l <Esc>:call FindSameIndent(0)<CR>m'gv''
vnoremap <silent> il <Esc>:call IndentInnerMotion(v:count1)<CR>
vnoremap <silent> al <Esc>:call IndentOuterMotion(v:count1)<CR>
vnoremap <silent> ]L <Esc>:call FindOutIndent(1)<CR>m'gv''
vnoremap <silent> [L <Esc>:call FindOutIndent(0)<CR>m'gv''
onoremap <silent> ]l :call FindSameIndent(1)<CR>
onoremap <silent> [l :call FindSameIndent(0)<CR>
onoremap <silent> ]L :call FindOutIndent(1)<CR>
onoremap <silent> [L :call FindOutIndent(0)<CR>
onoremap <silent> il :call IndentInnerMotion(v:count1)<CR>
onoremap <silent> al :call IndentOuterMotion(v:count1)<CR>

autocmd BufWinEnter * call s:IfNERDTreeOpenGotoFile()

function! s:IfNERDTreeOpenGotoFile()
    if exists("t:NERDTreeBufName")
        if bufwinnr(t:NERDTreeBufName) != -1
            if !empty(expand("%"))
                NERDTreeFind
            endif
        endif
    endif
endfunction

augroup suffixes
    autocmd!

    let associations = [
                \["javascript", ".js,.javascript,.es,.esx,.json"],
                \["python", ".py,.pyw"]
                \]

    for ft in associations
        execute "autocmd FileType " . ft[0] . " setlocal suffixesadd=" . ft[1]
    endfor
augroup END
autocmd FileType javascript setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
autocmd FileType pug setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window if the only
" buffer that's left is the NERDTree buffer
function! s:CloseIfOnlyNerdTreeLeft()
  if exists("t:NERDTreeBufName")
    if bufwinnr(t:NERDTreeBufName) != -1
      if winnr("$") == 1
        q
      endif
    endif
  endif
endfunction

function! s:FindInDir(dir)
  execute "vimgrep /" . expand("<cword>") . "/j " . a:dir . "/**/*." . expand("%:e")
  copen
endfunction

com -nargs=1 FindInDir call s:FindInDir(<f-args>)

nmap <F3> #:FindInDir<Space>


set backspace=indent,eol,start
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="horizontal"
let g:UltiSnipsSnippetsDir = "~/.vim/UltiSnips/"

hi clear SpellBad
hi SpellBad cterm=underline

set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1
